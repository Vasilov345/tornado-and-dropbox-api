import os.path
import uuid

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options

import dropbox
import urllib.request

from settings import DROPBOX_APP_KEY, DROPBOX_SECRET_KEY

from tornado.options import define, options

define("port", default=8000, help="run on the given port", type=int)

__UPLOADS__ = 'uploads/'


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
        ]

        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            debug=False,
            xsrf_cookies=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):

    def get(self):
        flow = dropbox.client.DropboxOAuth2FlowNoRedirect(DROPBOX_APP_KEY, DROPBOX_SECRET_KEY)
        authorize_url = flow.start()
        self.render(
            "upload_file.html", authorize_url=authorize_url
        )

    def post(self):
        result = {
            'status': 'Success',
            'message': "File successfully uploaded"
        }

        url = self.get_argument("file_url")
        file_name = self.get_argument("file_name")
        code = self.get_argument("code")

        result, destination = self.download_file_from_url(url, file_name, result)
        if result['status'] == "Success":
            result = self.upload_file_on_dropbox(code, destination, file_name, result)
        self.render(
            "result.html", result=result
        )

    @staticmethod
    def upload_file_on_dropbox(code, destination, file_name, result):
        try:
            flow = dropbox.client.DropboxOAuth2FlowNoRedirect(DROPBOX_APP_KEY, DROPBOX_SECRET_KEY)
            access_token, user_id = flow.finish(code)
            client = dropbox.Dropbox(access_token)
            f = open(destination, 'rb')
            client.files_upload(f, '/' + file_name)
        except dropbox.client.ErrorResponse:
            result = {'status': "Failed",
                      "message": "Wrong Code. Maybe it is expired. Try to reload page and get code one more time"}
        except dropbox.exceptions.ApiError:
            result = {'status': "Failed",
                      "message": "Conflict in path. Try to change file name.Maybe there is another file with such name"}
        except ConnectionError:
            result = {'status': "Failed",
                      "message": "Connection reset by peer"}
        except Exception as e:
            result = {'status': "Failed",
                      "message": e}
        return result

    @staticmethod
    def download_file_from_url(url, file_name, result):
        destination = __UPLOADS__ + str(uuid.uuid1()) + '-' + file_name
        try:
            urllib.request.urlretrieve(url, destination)
        except ValueError:
            result = {'status': "Failed",
                      "message": "Unknown type of url. Try to check url for file download"}
        return result, destination


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
