import os
from main import Application, MainHandler
from settings import TESTING,  DROPBOX_APP_KEY, DROPBOX_SECRET_KEY
import unittest2 as unittest

import uuid
import dropbox


class DropboxTestCase(unittest.TestCase):
    def get_app(self):
        return Application()

    def setUp(self):
        self.file_path = TESTING["path"]
        self.test_url = TESTING["url"]
        self.file_name = TESTING["name"]
        self.result = {
            'status': 'Success',
            'message': "File successfully uploaded"
        }

    def test_download_from_url(self):
        result, destination = MainHandler.download_file_from_url(self.test_url, self.file_name, self.result)
        print(destination)
        self.assertEqual(result['status'], self.result['status'])
        os.remove(destination)

    def test_upload_on_dropbox(self):
        app_key = DROPBOX_APP_KEY
        app_secret = DROPBOX_SECRET_KEY

        flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)

        authorize_url = flow.start()
        print('1. Go to: ' + authorize_url)
        print('2. Click "Allow" (you might have to log in first)')
        print('3. Copy the authorization code.')
        code = input("Enter the authorization code here: ").strip()

        result = MainHandler.upload_file_on_dropbox(code, self.file_path, str(uuid.uuid4()), self.result)
        self.assertEqual(result['status'], self.result['status'])

if __name__ == '__main__':
    unittest.main()
